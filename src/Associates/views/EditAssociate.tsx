import { connect } from "react-redux";
import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { useState } from 'react';
import Chip from '@material-ui/core/Chip';
import { Associates, Specialization } from '../interfaces';
import { useHistory } from "react-router-dom";

import withFetchData from "../..//utils/withFetchData";
import { getSpecializations } from "../../Specializations/store/selectors";
import { fetchSpecializationsAction } from '../../Specializations/store/actions';
import { fetchSingleAssociateAction, fetchUpdateAssociateAction } from '../../Associates/store/actions';
import { useEffect } from "react";
import Snackbar from '@material-ui/core/Snackbar';
import { getSingleAssociate, isCreating } from "../store/selectors";
import CircularProgress from '@material-ui/core/CircularProgress';


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      minWidth: 275,
      width: '100%',
      margin: "auto",
      display: "flex",
      flexDirection: "column"
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    formRoot: {
      '& > *': {
        margin: theme.spacing(1),
        width: '25ch',
      },
    },
    card: {},

    chip: {
      margin: theme.spacing(0.5),

    },
  })
)


export const mapStateToProps = (state: any) => ({
  specializations: getSpecializations(state),
  associate: getSingleAssociate(state)
  // isCreating: isCreating(state)
})

export const mapDispatchToProps = (dispatch: any) => ({
  fetchData: () => {
    dispatch(fetchSpecializationsAction());
  },
  fetchSingleAssociate: (associateId: string) => {
    dispatch(fetchSingleAssociateAction({ associateId: associateId }))
  },
  updateAssociate: (associate: Associates) => dispatch(fetchUpdateAssociateAction(associate))
});

function EditAssociate(props: any) {
  const classes = useStyles();
  const {
    specializations,
    updateAssociate,
    fetchSingleAssociate,
    associate,
    match: { params: { associateId } }
  } = props
  const [name, setName] = useState('')
  const [phone, setPhone] = useState('')
  const [address, setAddress] = useState('')
  const [chipData, setChipData] = React.useState<Specialization[]>(specializations);
  const [showError, setShowError] = useState(false)
  const history = useHistory();

  const mapData = () => {
    if (associate && associate._id) {
      const { associateName: name, phone, address, specializations } = associate
      setName(name)
      setPhone(phone)
      setAddress(address)
      let allSpecs = chipData

      specializations.map((selectedSpec: any) => {
        const idx = allSpecs.findIndex(incSpec => incSpec._id === selectedSpec._id)
        if (idx > -1) {
          allSpecs[idx].selected = true
        }
      })
      setChipData(allSpecs)
    }
  }

  useEffect(() => {
    mapData()
  }, [associate])

  useEffect(() => {
    fetchSingleAssociate(associateId)
  }, [])

  useEffect(() => {
    setChipData(specializations)
  }, [specializations])

  const toggleSelect = (specialization: Specialization,) => {
    setChipData(
      chipData.map(item =>
        item._id === specialization._id
          ? { ...item, selected: !item.selected }
          : item
      ))
  };

  const Validate = () => {
    const selectedSpecialization = chipData.filter((chip: Specialization) => chip.selected === true).map(selectedChip => selectedChip._id)
    if (name.length && phone.toString().length && address.length && selectedSpecialization.length) {
      return onSubmit(selectedSpecialization)
    }
    setShowError(true)
  }

  const onSubmit = async (specializations: any) => {
    console.log({ associateName: name, phone, address, specializations })
    await updateAssociate({ id: associateId, associateName: name, phone, address, specializations })
    history.goBack()
  }
  const updateField =
    (value: string, setFun: Function) => {
      setFun(value)
      setShowError(false)
    }


  return (
    <Card className={classes.root} variant="outlined" >
      <CardContent className={classes.root} >
        <form className={classes.formRoot} noValidate autoComplete="off">

          <TextField
            id="filled-basic"
            label="Name"
            error={!name.length && showError}
            variant="filled"
            value={name}
            fullWidth
            helperText={!name.length && showError && "Enter the name of associate."}
            onChange={(e) => updateField(e.target.value, setName)}
          />
          <TextField
            id="filled-basic"
            label="Phone"
            variant="filled"
            type="number"
            error={!phone.toString().length && showError}
            value={phone}
            fullWidth
            helperText={!phone.toString().length && showError && "Enter phone number"}
            onChange={(e) => updateField(e.target.value, setPhone)}
          />

          <TextField
            id="filled-multiline-static"
            label="Address"
            multiline
            rows={4}
            error={!address.length && showError}
            variant="filled"
            helperText={!address.length && showError && "Enter address"}
            fullWidth
            value={address}
            onChange={(e) => updateField(e.target.value, setAddress)}
          />
          {chipData.map((data) => {
            return (
              // <li key={data._id}>
              <Chip
                key={data._id}
                label={data.specializationName}
                onClick={() => toggleSelect(data)}
                color={data.selected ? "secondary" : "default"}
                className={classes.chip}
              />
              // </li>
            );
          })}
          <Snackbar
            open={!address.length && showError}
            message="Select atleast one specialization"
          />
          <Button
            variant="contained"
            color="primary"
            onClick={Validate} >
            Update Associate
          </Button>

        </form>
      </CardContent>
    </Card >
  )
}
export default connect(mapStateToProps, mapDispatchToProps)(withFetchData(EditAssociate));
