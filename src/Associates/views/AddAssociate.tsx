import { connect } from "react-redux";
import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { useState } from 'react';
import Chip from '@material-ui/core/Chip';
import { Associates, Specialization } from '../interfaces';
import { useHistory } from "react-router-dom";

import withFetchData from "../..//utils/withFetchData";
import { getSpecializations } from "../../Specializations/store/selectors";
import { fetchSpecializationsAction } from '../../Specializations/store/actions';
import { fetchCreateAssociateAction } from '../../Associates/store/actions';
import { useEffect } from "react";
import Snackbar from '@material-ui/core/Snackbar';
import { isCreating } from "../store/selectors";
import CircularProgress from '@material-ui/core/CircularProgress';


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      minWidth: 275,
      width: '100%',
      margin: "auto",
      display: "flex",
      flexDirection: "column"
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    formRoot: {
      '& > *': {
        margin: theme.spacing(1),
        width: '25ch',
      },
    },
    card: {},

    chip: {
      margin: theme.spacing(0.5),

    },
  })
)


export const mapStateToProps = (state: any) => ({
  specializations: getSpecializations(state),
  isCreating: isCreating(state)
})

export const mapDispatchToProps = (dispatch: any) => ({
  fetchData: () => {
    dispatch(fetchSpecializationsAction());
  },
  createAssociate: (associate: Associates) =>
    dispatch(fetchCreateAssociateAction(associate))
});

function AddAssociate(props: any) {
  const classes = useStyles();
  const { specializations, createAssociate, isCreating } = props
  const [name, setName] = useState('')
  const [phone, setPhone] = useState('')
  const [address, setAddress] = useState('')
  const [chipData, setChipData] = React.useState<Specialization[]>(specializations);
  const [showError, setShowError] = useState(false)
  const history = useHistory();

  useEffect(() => {
    setChipData(specializations)
  }, [specializations])

  const toggleSelect = (specialization: Specialization,) => {
    setChipData(
      chipData.map(item =>
        item._id === specialization._id
          ? { ...item, selected: !item.selected }
          : item
      ))
  };

  const Validate = () => {
    const selectedSpecialization = chipData.filter((chip: Specialization) => chip.selected === true).map(selectedChip => selectedChip._id)
    if (name && phone && address && selectedSpecialization.length) {
      return onSubmit(selectedSpecialization)
    }
    setShowError(true)
  }

  const onSubmit = async (specializations: any) => {
    await createAssociate({ associateName: name, phone, address, specializations })
    history.goBack()
  }
  const updateField =
    (value: string, setFun: Function) => {
      setFun(value)
    }

  if (isCreating) {
    return <CircularProgress disableShrink />;
  }

  return (
    <Card className={classes.root} variant="outlined" >
      <CardContent className={classes.root} >
        <form className={classes.formRoot} noValidate autoComplete="off">

          <TextField
            id="filled-basic"
            label="Name"
            error={!name.length && showError}
            variant="filled"
            value={name}
            fullWidth
            helperText={!name.length && showError && "Enter the name of associate."}
            onChange={(e) => updateField(e.target.value, setName)}
          />
          <TextField
            id="filled-basic"
            label="Phone"
            variant="filled"
            type="number"
            error={!phone.length && showError}
            value={phone}
            fullWidth
            helperText={!phone.length && showError && "Enter phone number"}
            onChange={(e) => updateField(e.target.value, setPhone)}
          />

          <TextField
            id="filled-multiline-static"
            label="Address"
            multiline
            rows={4}
            error={!address.length && showError}
            variant="filled"
            helperText={!address.length && showError && "Enter address"}
            fullWidth
            value={address}
            onChange={(e) => updateField(e.target.value, setAddress)}
          />
          {chipData.map((data) => {
            return (
              // <li key={data._id}>
              <Chip
                key={data._id}
                label={data.specializationName}
                onClick={() => toggleSelect(data)}
                color={data.selected ? "secondary" : "default"}
                className={classes.chip}
              />
              // </li>
            );
          })}
          <Snackbar
            open={!address.length && showError}
            message="Select atleast one specialization"
          />
          <Button
            variant="contained"
            color="primary"
            onClick={Validate} >
            Add Associate
          </Button>

        </form>
      </CardContent>
    </Card >
  )
}
export default connect(mapStateToProps, mapDispatchToProps)(withFetchData(AddAssociate));
