import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { useState } from 'react';
import { connect } from "react-redux";
import { Associates } from '../interfaces';
import { fetchSearchAssociatesAction } from '../store/actions';
import { searchResults } from '../store/selectors';

export const mapStateToProps = (state: any) => ({
  results: searchResults(state)
})
export const mapDispatchToProps = (dispatch: any) => ({
  searchAssociate: (searchTerm: string) => {
    console.log('searchTerm', searchTerm)
    dispatch(fetchSearchAssociatesAction({ searchTerm: searchTerm }))
  }
});
interface PropTypes {
  searchAssociate: (searchTerm: string) => void,
  results: Associates[]
}
function SearchAssociate(props: PropTypes) {
  const { searchAssociate, results } = props
  const [pendingValue, setPendingValue] = useState<Associates | null>(null)
  return (
    <Autocomplete
      id="search-associate"
      options={results}
      getOptionLabel={(option: Associates) => option.associateName}
      style={{ width: 300 }}
      value={pendingValue}
      onChange={(event: any, newValue: any) => {
        setPendingValue(newValue)
      }}

      renderInput={(params: any) => <TextField {...params} label="Search Associate" onChange={(e: any) => searchAssociate(e.target.value)} variant="outlined" />}
    />
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchAssociate);
