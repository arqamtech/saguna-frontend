import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import { Associates, Specialization } from '../interfaces'
import Button from '@material-ui/core/Button';

import withFetchData from "../..//utils/withFetchData";
import { fetchAssociatesAction, fetchDeleteAssociateAction } from "../store/actions";
import { getAssociates } from "../store/selectors";


import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import SearchAssociate from "./SearchAssociate";



export const mapStateToProps = (state: any) => ({
  associates: getAssociates(state)
})

export const mapDispatchToProps = (dispatch: any) => ({
  fetchData: () => {
    dispatch(fetchAssociatesAction());
  },
  deleteAssociate: (associateId: string) => {
    dispatch(fetchDeleteAssociateAction({ associateId: associateId }));
  }
});

const ListAssociates = (props: any) => {
  const { associates, deleteAssociate } = props;
  const history = useHistory();
  const GotoCreateAssociate = () => history.push(`/add`)
  const GotoEditAssociate = (associateId: any) => history.push(`/associate/${associateId}`)
  return (
    <>
      <SearchAssociate />
      <Button variant="contained" color="primary" onClick={GotoCreateAssociate} >
        Add Associate
      </Button>

      <TableContainer component={Paper}>
        <Table size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell align="left">Phone</TableCell>
              <TableCell align="left">Address</TableCell>
              <TableCell align="left">Specializations</TableCell>
              <TableCell align="left">Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {associates.map((row: Associates) => (
              <TableRow key={row._id}>
                <TableCell component="th" scope="row">
                  {row.associateName}
                </TableCell>
                <TableCell align="left">{row.phone}</TableCell>
                <TableCell align="left">{row.address}</TableCell>
                <TableCell align="left">
                  {row.specializations.map(({ _id, specializationName }: Specialization) => {
                    return (
                      <li key={_id}>{specializationName}</li>
                    )
                  })}
                </TableCell>
                <TableCell align="left">

                  <Button variant="contained" color="primary"
                    onClick={() => GotoEditAssociate(row._id)} >
                    Edit
                  </Button>
                  <Button variant="contained" color="primary" onClick={() => deleteAssociate(row._id)}>
                    Delete
                  </Button>


                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>

  );
};

export default connect(mapStateToProps, mapDispatchToProps)(withFetchData(ListAssociates));
