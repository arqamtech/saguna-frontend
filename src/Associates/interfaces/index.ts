export interface Associates {
  _id?: string;
  associateName: string;
  phone: number;
  address: string;
  specializations: Array<Specialization>;
}

export interface Specialization {
  _id?: string;
  specializationName: string;
  selected?: boolean;
}
