import { call, put, takeLatest } from "redux-saga/effects";
import log from "../../../utils/logger";
import api from "../../../utils/api/index";
import { ApiResponse } from "../../../Interfaces";
import {
  // Fetch All Associates
  fetchAssociatesAction,
  fetchAssociatesStart,
  fetchAssociatesError,
  fetchAssociatesSuccess,
  // Search Associate
  fetchSearchAssociatesAction,
  fetchSearchAssociatesStart,
  fetchSearchAssociatesSuccess,
  fetchSearchAssociatesError,
  // Single Associate
  fetchSingleAssociateAction,
  fetchSingleAssociateStart,
  fetchSingleAssociateSuccess,
  fetchSingleAssociateError,
  // Delete Associate
  fetchDeleteAssociateAction,
  fetchDeleteAssociateStart,
  fetchDeleteAssociateSuccess,
  fetchDeleteAssociateError,
} from "../actions";
import {
  errorRetrievingAssociates,
  errorDeletingAssociate,
  errorRetrievingAssociate,
  errorSearchingAssociate,
} from "../../../utils/messages";
function* fetchAssociates() {
  try {
    yield put(fetchAssociatesStart());
    const response: ApiResponse = yield call(api.getAssociates);
    yield put(fetchAssociatesSuccess(response));
  } catch (error) {
    yield put(fetchAssociatesError(error));
    yield log(errorRetrievingAssociates, error);
  }
}

function* fetchSearchAssociates({ searchTerm }: { searchTerm: string }) {
  try {
    yield put(fetchSearchAssociatesStart());
    const response: ApiResponse = yield call(api.searchAssociates, searchTerm);
    yield put(fetchSearchAssociatesSuccess(response));
  } catch (error) {
    yield put(fetchSearchAssociatesError(error));
    yield log(errorSearchingAssociate, error);
  }
}

function* fetchSingleAssociate({ associateId }: { associateId: string }) {
  try {
    yield put(fetchSingleAssociateStart());
    const response: ApiResponse = yield call(
      api.getSingleAssociate,
      associateId
    );
    yield put(fetchSingleAssociateSuccess(response));
  } catch (error) {
    yield put(fetchSingleAssociateError(error));
    yield log(errorRetrievingAssociate, error);
  }
}

function* fetchDeleteAssociate({ associateId }: { associateId: string }) {
  try {
    yield put(fetchDeleteAssociateStart());
    const response: ApiResponse = yield call(api.deleteAssociate, associateId);
    yield put(fetchDeleteAssociateSuccess(response));
    yield put(fetchAssociatesAction());
  } catch (error) {
    yield put(fetchDeleteAssociateError(error));
    yield log(errorDeletingAssociate, error);
  }
}

function* watchfetchGetAssociates() {
  yield takeLatest(fetchAssociatesAction().type, fetchAssociates);
}

function* watchfetchSearchAssociates() {
  yield takeLatest(fetchSearchAssociatesAction().type, fetchSearchAssociates);
}

function* watchfetchSingleAssociates() {
  yield takeLatest(fetchSingleAssociateAction().type, fetchSingleAssociate);
}

function* watchfetchDeleteAssociates() {
  yield takeLatest(fetchDeleteAssociateAction().type, fetchDeleteAssociate);
}

const watchers = [
  watchfetchGetAssociates(),
  watchfetchSearchAssociates(),
  watchfetchSingleAssociates(),
  watchfetchDeleteAssociates(),
];
export default watchers;
