import { call, put, takeLatest } from "redux-saga/effects";
import api from "../../../utils/api/index";
import { Associates } from "../../interfaces";
import {
  // Create Associate
  fetchCreateAssociateAction,
  fetchCreateAssociateStart,
  fetchCreateAssociateSuccess,
  fetchCreateAssociateError,
  // Update Associate
  fetchUpdateAssociateAction,
  fetchUpdateAssociateStart,
  fetchUpdateAssociateSuccess,
  fetchUpdateAssociateError,
} from "../actions";
import { errorCreatingAssociate } from "../../../utils/messages";
import { ReduxInterface } from "../../../Interfaces";

function* createAssociate(data: Associates) {
  try {
    yield put(fetchCreateAssociateStart());
    const response: ReduxInterface = yield call(api.createAssociate, data);
    yield put(fetchCreateAssociateSuccess(response));
  } catch (error) {
    yield put(fetchCreateAssociateError());
    console.log(errorCreatingAssociate);
  }
}

function* updateAssociate(data: Associates) {
  try {
    yield put(fetchUpdateAssociateStart());
    const response: ReduxInterface = yield call(api.updateAssociate, data);
    yield put(fetchUpdateAssociateSuccess(response));
  } catch (error) {
    yield put(fetchUpdateAssociateError());
    console.log(errorCreatingAssociate);
  }
}

function* watchCreateAssociate() {
  yield takeLatest(fetchCreateAssociateAction().type, createAssociate);
}

function* watchUpdateAssociate() {
  yield takeLatest(fetchUpdateAssociateAction().type, updateAssociate);
}

const watchers = [watchCreateAssociate(), watchUpdateAssociate()];
export default watchers;
