import { get } from "lodash";

// Fetching  Associate
export const getAssociates = (state: any) =>
  get(state, ["associates", "getAssociates", "data"]);
export const isFetching = (state: any) =>
  get(state, ["associates", "getAssociates", "isFetching"]);

// Creating Associate
export const isCreating = (state: any) =>
  get(state, ["associates", "createAssociate", "isCreating"]);
export const isCreatingError = (state: any) =>
  get(state, ["associates", "createAssociate", "isError"]);
export const createdAccount = (state: any) =>
  get(state, ["associates", "createAssociate", "account"]);

// Search Associate
export const searchResults = (state: any) =>
  get(state, ["associates", "searchAssociates", "data"]);

// Single Associate
export const getSingleAssociate = (state: any) =>
  get(state, ["associates", "getSingleAssociate", "data"]);
