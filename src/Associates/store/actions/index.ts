import { createFetchActions } from "../../../helpers/actions/creators";

export const ASSOCIATES = "ASSOCIATES";
export const CREATE_ASSOCIATE = "CREATE_ASSOCIATE";
export const SEARCH_ASSOCIATES = "SEARCH_ASSOCIATES";
export const SINGLE_ASSOCIATE = "SINGLE_ASSOCIATE";
export const UPDATE_ASSOCIATE = "UPDATE_ASSOCIATE";
export const DELETE_ASSOCIATE = "DELETE_ASSOCIATE";

export const {
  fetchAction: fetchAssociatesAction,
  fetchStartAction: fetchAssociatesStart,
  fetchSuccessAction: fetchAssociatesSuccess,
  fetchErrorAction: fetchAssociatesError,
} = createFetchActions(ASSOCIATES);

export const {
  fetchAction: fetchSingleAssociateAction,
  fetchStartAction: fetchSingleAssociateStart,
  fetchSuccessAction: fetchSingleAssociateSuccess,
  fetchErrorAction: fetchSingleAssociateError,
} = createFetchActions(SINGLE_ASSOCIATE);

export const {
  fetchAction: fetchSearchAssociatesAction,
  fetchStartAction: fetchSearchAssociatesStart,
  fetchSuccessAction: fetchSearchAssociatesSuccess,
  fetchErrorAction: fetchSearchAssociatesError,
} = createFetchActions(SEARCH_ASSOCIATES);

export const {
  fetchAction: fetchCreateAssociateAction,
  fetchStartAction: fetchCreateAssociateStart,
  fetchSuccessAction: fetchCreateAssociateSuccess,
  fetchErrorAction: fetchCreateAssociateError,
} = createFetchActions(CREATE_ASSOCIATE);

export const {
  fetchAction: fetchUpdateAssociateAction,
  fetchStartAction: fetchUpdateAssociateStart,
  fetchSuccessAction: fetchUpdateAssociateSuccess,
  fetchErrorAction: fetchUpdateAssociateError,
} = createFetchActions(UPDATE_ASSOCIATE);

export const {
  fetchAction: fetchDeleteAssociateAction,
  fetchStartAction: fetchDeleteAssociateStart,
  fetchSuccessAction: fetchDeleteAssociateSuccess,
  fetchErrorAction: fetchDeleteAssociateError,
} = createFetchActions(DELETE_ASSOCIATE);
