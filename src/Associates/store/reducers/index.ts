import getAssociates from "./getAssociates";
import createAssociate from "./createAssociate";
import searchAssociates from "./searchAssociates";
import getSingleAssociate from "./getSingleAssociate";
import updateAssociate from "./updateAssociate";
import deleteAssociate from "./deleteAssociate";
import { combineReducers } from "redux";

const associateReducer = combineReducers({
  createAssociate,
  getAssociates,
  searchAssociates,
  getSingleAssociate,
  updateAssociate,
  deleteAssociate,
});
export default associateReducer;
