import {
  fetchDeleteAssociateStart,
  fetchDeleteAssociateSuccess,
  fetchDeleteAssociateError,
} from "../actions";
import { ReduxInterface } from "../../../Interfaces";

const initialState = {
  isDeleting: false,
  isError: "",
  account: {},
};
const deleteAssociate = (
  state = initialState,
  { type, payload, error }: ReduxInterface
) => {
  switch (type) {
    case fetchDeleteAssociateStart().type: {
      return {
        ...state,
        isDeleting: true,
      };
    }
    case fetchDeleteAssociateSuccess().type: {
      return {
        ...state,
        isDeleting: false,
        account: payload,
      };
    }
    case fetchDeleteAssociateError().type:
      return {
        ...state,
        isDeleting: false,
        isError: error,
      };
    default:
      return state;
  }
};
export default deleteAssociate;
