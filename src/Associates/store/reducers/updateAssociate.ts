import {
  fetchUpdateAssociateStart,
  fetchUpdateAssociateSuccess,
  fetchUpdateAssociateError,
} from "../actions";
import { ReduxInterface } from "../../../Interfaces";

const initialState = {
  isCreating: false,
  isError: "",
  account: {},
};
const updateAssociate = (
  state = initialState,
  { type, payload, error }: ReduxInterface
) => {
  switch (type) {
    case fetchUpdateAssociateStart().type: {
      return {
        ...state,
        isCreating: true,
      };
    }
    case fetchUpdateAssociateSuccess().type: {
      return {
        ...state,
        isCreating: false,
        account: payload,
      };
    }
    case fetchUpdateAssociateError().type:
      return {
        ...state,
        isCreating: false,
        isError: error,
      };
    default:
      return state;
  }
};
export default updateAssociate;
