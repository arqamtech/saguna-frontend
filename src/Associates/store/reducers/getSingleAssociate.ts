import {
  fetchSingleAssociateStart,
  fetchSingleAssociateSuccess,
  fetchSingleAssociateError,
} from "../actions";
import { ReduxInterface } from "../../../Interfaces";
const initialState = {
  isFetching: false,
  data: {},
};
const getSingleAssociate = (
  state = initialState,
  { type, payload }: ReduxInterface
) => {
  switch (type) {
    case fetchSingleAssociateStart().type:
      return {
        ...state,
        isFetching: true,
      };
    case fetchSingleAssociateSuccess().type: {
      return {
        ...state,
        isFetching: false,
        data: payload.data,
      };
    }
    case fetchSingleAssociateError().type:
      return {
        ...state,
        isFetching: false,
      };
    default:
      return state;
  }
};
export default getSingleAssociate;
