import {
  fetchSearchAssociatesStart,
  fetchSearchAssociatesSuccess,
  fetchSearchAssociatesError,
} from "../actions";
import { ReduxInterface } from "../../../Interfaces";
const initialState = {
  isFetching: false,
  data: [],
};
const searchAssociates = (
  state = initialState,
  { type, payload }: ReduxInterface
) => {
  switch (type) {
    case fetchSearchAssociatesStart().type:
      return {
        ...state,
        isFetching: true,
      };
    case fetchSearchAssociatesSuccess().type: {
      return {
        ...state,
        isFetching: false,
        data: payload.data,
      };
    }
    case fetchSearchAssociatesError().type:
      return {
        ...state,
        isFetching: false,
      };
    default:
      return state;
  }
};
export default searchAssociates;
