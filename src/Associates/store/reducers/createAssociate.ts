import {
  fetchCreateAssociateStart,
  fetchCreateAssociateSuccess,
  fetchCreateAssociateError,
} from "../actions";
import { ReduxInterface } from "../../../Interfaces";

const initialState = {
  isCreating: false,
  isError: "",
  account: {},
};
const createAssociate = (
  state = initialState,
  { type, payload, error }: ReduxInterface
) => {
  switch (type) {
    case fetchCreateAssociateStart().type: {
      return {
        ...state,
        isCreating: true,
      };
    }
    case fetchCreateAssociateSuccess().type: {
      return {
        ...state,
        isCreating: false,
        account: payload,
      };
    }
    case fetchCreateAssociateError().type:
      return {
        ...state,
        isCreating: false,
        isError: error,
      };
    default:
      return state;
  }
};
export default createAssociate;
