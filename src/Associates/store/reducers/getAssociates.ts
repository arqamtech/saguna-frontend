import {
  fetchAssociatesStart,
  fetchAssociatesSuccess,
  fetchAssociatesError,
} from "../actions";
import { ReduxInterface } from "../../../Interfaces";
const initialState = {
  isFetching: false,
  data: [],
};
const getAssociates = (
  state = initialState,
  { type, payload }: ReduxInterface
) => {
  switch (type) {
    case fetchAssociatesStart().type:
      return {
        ...state,
        isFetching: true,
      };
    case fetchAssociatesSuccess().type: {
      return {
        ...state,
        isFetching: false,
        data: payload.data,
      };
    }
    case fetchAssociatesError().type:
      return {
        ...state,
        isFetching: false,
      };
    default:
      return state;
  }
};
export default getAssociates;
