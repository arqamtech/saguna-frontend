export interface ApiResponse {
  status: string;
  data: [];
}

export interface ReduxInterface {
  type: string;
  payload: ApiResponse;
  error?: string;
}
