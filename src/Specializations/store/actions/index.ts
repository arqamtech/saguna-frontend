import { createFetchActions } from "../../../helpers/actions/creators";

export const SPECIALIZATIONS = "SPECIALIZATIONS";

export const {
  fetchAction: fetchSpecializationsAction,
  fetchStartAction: fetchSpecializationsStart,
  fetchSuccessAction: fetchSpecializationsSuccess,
  fetchErrorAction: fetchSpecializationsError,
} = createFetchActions(SPECIALIZATIONS);
