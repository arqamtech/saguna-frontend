import { get } from "lodash";

// Fetching  Cluster
export const getSpecializations = (state: any) =>
  get(state, ["specializations", "getSpecializations", "data"]);
export const isFetching = (state: any) =>
  get(state, ["specializations", "getSpecializations", "isFetching"]);
