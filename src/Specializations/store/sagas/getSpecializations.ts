import { call, put, takeLatest } from "redux-saga/effects";
import log from "../../../utils/logger";
import api from "../../../utils/api/index";
import { ApiResponse } from "../../../Interfaces";
import {
  fetchSpecializationsAction,
  fetchSpecializationsStart,
  fetchSpecializationsError,
  fetchSpecializationsSuccess,
} from "../actions";
import { errorRetrievingSpecializations } from "../../../utils/messages";
function* fetchSpecializations() {
  try {
    yield put(fetchSpecializationsStart());
    const response: ApiResponse = yield call(api.getSpecializations);
    yield put(fetchSpecializationsSuccess(response));
  } catch (error) {
    yield put(fetchSpecializationsError(error));
    yield log(errorRetrievingSpecializations, error);
  }
}

function* watchfetchSpecializations() {
  yield takeLatest(fetchSpecializationsAction().type, fetchSpecializations);
}

const watchers = [watchfetchSpecializations()];
export default watchers;
