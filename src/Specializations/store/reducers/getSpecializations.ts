import {
  fetchSpecializationsStart,
  fetchSpecializationsSuccess,
  fetchSpecializationsError,
} from "../actions";
import { ReduxInterface } from "../../../Interfaces";
import { Specialization } from "../../../Associates/interfaces";

const initialState = {
  isFetching: false,
  data: [],
};
const getSpecializations = (
  state = initialState,
  { type, payload }: ReduxInterface
) => {
  switch (type) {
    case fetchSpecializationsStart().type:
      return {
        ...state,
        isFetching: true,
      };
    case fetchSpecializationsSuccess().type: {
      return {
        ...state,
        isFetching: false,
        data: payload.data.map((item: Specialization) => ({
          selected: false,
          ...item,
        })),
      };
    }
    case fetchSpecializationsError().type:
      return {
        ...state,
        isFetching: false,
      };
    default:
      return state;
  }
};
export default getSpecializations;
