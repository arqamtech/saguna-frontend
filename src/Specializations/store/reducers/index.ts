import getSpecializations from "./getSpecializations";
import { combineReducers } from "redux";

const specializationReducer = combineReducers({
  getSpecializations,
});
export default specializationReducer;
