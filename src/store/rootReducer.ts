import { combineReducers } from "redux";
import flowRight from "lodash/flowRight";
import withPersist from "./withPersist";
import appPersistConfig from "./persistConfig";
import associates from "../Associates/store/reducers";
import specializations from "../Specializations/store/reducers";

const appEnhancer = flowRight(withPersist(appPersistConfig));

const rootReducer = (state: any, action: any) =>
  combineReducers({
    associates,
    specializations,
  })(state, action);

export default appEnhancer(rootReducer);
