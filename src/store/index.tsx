import { createStore, applyMiddleware, compose } from "redux";
import { createLogger } from "redux-logger";
import createSagaMiddleware from "redux-saga";
import { persistStore } from "redux-persist";
import { setupAxios } from "../utils/api/axios";
import rootReducer from "./rootReducer";
import startSagas from "./sagas";

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}
export default function configureStore() {
  const sagaMiddleware = createSagaMiddleware();
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(...getMiddleware(sagaMiddleware)))
  );
  const persistor = persistStore(store);
  sagaMiddleware.run(startSagas);
  setupAxios(store);
  return { store, persistor };
}

const getMiddleware = (sagaMiddleware: any) => {
  if (process.env.NODE_ENV === "development") {
    const loggerMiddleware = createLogger({
      collapsed: true,
      diff: false,
    });
    return [sagaMiddleware, loggerMiddleware];
  }
  return [sagaMiddleware];
};
