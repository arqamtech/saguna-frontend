import localForage from "localforage";

const appPersistConfig = {
  key: "test_data",
  storage: localForage,
  whitelist: ["dashboard", "users"],
  timeout: 0,
};

export default appPersistConfig;
