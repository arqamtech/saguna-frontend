import { all } from "redux-saga/effects";
import log from "../utils/logger";
import getAssociateSagas from "../Associates/store/sagas/getAssociates";
import createAssociateSagas from "../Associates/store/sagas/createAssociate";
import getSpecializationSagas from "../Specializations/store/sagas/getSpecializations";

export default function* startSagas() {
  log("Starting SAGAS");
  yield all([
    ...getAssociateSagas,
    ...createAssociateSagas,
    ...getSpecializationSagas,
  ]);
}
