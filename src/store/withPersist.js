import { persistReducer } from "redux-persist";

const persistReducerFunction = (persistConfig) => (reducer) => persistReducer(persistConfig, reducer);
export default persistReducerFunction;
