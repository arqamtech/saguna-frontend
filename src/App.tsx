import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import ListAssociates from './Associates/views/ListAssociates'
import AddAssociate from './Associates/views/AddAssociate'
import { connect } from 'react-redux';
import EditAssociate from './Associates/views/EditAssociate';

function App() {
  return (
    <HashRouter>
      <Switch>
        <Route exact path="/" render={(props: any) => <ListAssociates {...props} />} />
        <Route exact path="/add" render={(props: any) => <AddAssociate {...props} />} />
        <Route exact path="/associate/:associateId" render={(props: any) => <EditAssociate {...props} />} />
      </Switch>
    </HashRouter>
  )
}

export default connect()(App);
