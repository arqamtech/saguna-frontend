import axios from "axios";
import { createHTTPHeader } from "./utils";

export const setupAxios = (store) => {
  axios.interceptors.response.use(
    (response) => response,
    async (error) => {
      const originalConfig = error.config;
      const { response } = error;

      if (response && response.status === 401 && !originalConfig._retry) {
        const headers = createHTTPHeader();
        Object.entries(headers).forEach(([k, v]) => (originalConfig.headers[k] = v));
        originalConfig._retry = true;
        window.location.reload();

        return axios(originalConfig);
      }
      return Promise.reject(error);
    },
  );
};
