// TODO token and app type should only be needed once
export function createHTTPHeader(authToken) {
  const token = authToken == null ? "test" : authToken;
  return {
    Authorization: `Bearer ${token}`,
    "X-Api-Version": 2,
  };
}

export function createHeaders() {
  return {
    "X-Api-Version": 2,
  };
}

export const buildAPIUrl = (base_url, params) => {
  let finalParams = "";
  if (typeof params === "object" && Object.keys(params).length > 0) {
    const parts = [];
    for (const key in params) {
      const item = params[key];

      if ((item && !Array.isArray(item)) || (Array.isArray(item) && item.length > 0)) {
        parts.push(`${key}=${item}`);
      }
    }
    finalParams = `?${parts.join("&")}`;
  } else {
    finalParams = `/${params}`;
  }

  if (finalParams === "") {
    return base_url;
  }

  return `${base_url}${finalParams}`;
};
