import axios from "axios";
import { get } from "lodash";
import { createHTTPHeader } from "./utils";
import log from "../logger";

const NETWORK_ERR_MSG = "Network error";
const LOCAL_ERR_MSG = "Something went wrong";
const UNKNOWN_ERR_MSG = "Unknown error";

let API_URL = "http://localhost:7000/";

axios.defaults.timeout = 50000;

const api = {
  configure: ({ apiUrl }) => {
    API_URL = apiUrl;
  },
  get: (url, headers) => axios
    .get(url, { headers })
    .then((response) => response.data)
    .catch(handleError),
  getWithHeaders: (url, headers) => axios
    .get(url, { headers })
    .then((response) => ({ data: response.data, headers: response.headers }))
    .catch(handleError),

  post: (url, body, headers = createHTTPHeader("")) => axios
    .post(url, body, { headers })
    .then((response) => response.data)
    .catch(handleError),
  delete: (url, headers) => axios
    .delete(url, { headers })
    .then((response) => response.data)
    .catch(handleError),
  put: (url, body, headers) => axios
    .put(url, body, { headers })
    .then((response) => response.data)
    .catch(handleError),





  getAssociates: () => {
    const url = getAssociatesUrl();

    return api.get(url, {});
  },
  getSpecializations: () => {
    const url = getSpecializationsUrl();

    return api.get(url, {});
  },
  createAssociate: (data) => {
    const url = getAssociatesUrl();

    return api.post(url, data, {});
  },
  updateAssociate: (data) => {
    const url = getAssociatesUrl();
    // return url
    return api.put(url, data, {});
  },
  searchAssociates: (searchTerm) => {
    const url = getSearchAssociateUrl(searchTerm);

    return api.get(url, {});
  },
  getSingleAssociate: (associateId) => {
    const url = getSingleAssociateUrl(associateId);

    return api.get(url, {});
  },
  deleteAssociate: (associateId) => {
    const url = getSingleAssociateUrl(associateId);

    return api.delete(url, {});
  },
};

const getAssociatesUrl = () => `${API_URL}associates`;
const getSpecializationsUrl = () => `${API_URL}specializations`;
const getSearchAssociateUrl = (searchTerm) => `${API_URL}search/${searchTerm}`;
const getSingleAssociateUrl = (associateId) => `${API_URL}associate/${associateId}`;


const handleError = (error) => {
  let statusCode;
  let message = "";
  if (error.response) {
    log(error.response.status, error.response.data);
    message = get(error, ["response", "data", "error"]);
    const errors = get(error, ["response", "data", "error"]);
    if (!message) {
      message = typeof errors !== "object" ? errors : "";
    }
    if (!message) {
      message = UNKNOWN_ERR_MSG;
    }
    statusCode = error.response.status;
  } else if (error.request) {
    // The request was made but no response was received
    message = NETWORK_ERR_MSG;
  } else {
    log("Error", error);
    message = LOCAL_ERR_MSG;
  }
  const err = new Error(message);
  if (statusCode) {
    err.statusCode = statusCode;
  }
  if (typeof error.response.errors === "object") {
    err.data = error.response.errors;
  }
  return Promise.reject({ err: err, message: message });
};

export default api;
