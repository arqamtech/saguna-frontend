import React from "react";



const withFetchData = (Component) => class extends React.Component {
  componentDidMount() {
    const { fetchData } = this.props;
    fetchData && fetchData(this.props);
  }

  componentDidUpdate(prevProps) {
    const { componentDidUpdate } = this.props;
    componentDidUpdate && componentDidUpdate(prevProps, this.props);
  }

  componentWillUnmount() {
    const { componentWillUnmount } = this.props;
    componentWillUnmount && componentWillUnmount();
  }

  render() {
    const { fetchData, ...props } = this.props;
    return <Component {...props} />;
  }
};

export default withFetchData;
