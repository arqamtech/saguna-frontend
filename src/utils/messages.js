export const errorRetrievingAssociates = "Error retrieving associates";
export const errorRetrievingAssociate = "Error retrieving associate";
export const errorSearchingAssociate = "Error searching associate";
export const errorDeletingAssociate = "Error deleting associates";
export const errorRetrievingSpecializations = "Error retrieving specializations";
export const errorCreatingAssociate = "Error creating  associate";
